#!/bin/bash
docker start mycluster-slave-1
docker start mycluster-slave-2
docker start mycluster-master

docker exec -it mycluster-master "/usr/local/hadoop/spark-services.sh"

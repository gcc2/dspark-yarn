# Docker hadoop yarn cluster for spark 2.4.1

## Build image
- Run `docker build -t spark2/spark-hadoop-cluster .`

## Start Cluster  
- Run `./startHadoopCluster.sh`

## Enter Master
- `docker exec -it mycluster-master bash`

### Run spark applications on cluster :
- spark-shell : `spark-shell --master yarn --deploy-mode client`
- spark : `spark-submit --master yarn --deploy-mode client or cluster --num-executors 2 --executor-memory 4G --executor-cores 4 --class org.apache.spark.examples.SparkPi $SPARK_HOME/examples/jars/spark-examples_2.11-2.4.1.jar`

- Access to Hadoop cluster Web UI : <container ip>:8088 
- Access to spark Web UI : <container ip>:8080
- Access to hdfs Web UI : <container ip>:50070
  
## Stop 
- `docker stop $(docker ps -a -q)`
- `docker rm $(docker ps -a -q)`
- `docker rmi $(docker ps -a -q)`
- `docker container prune`

## jupyter
- `jupyter notebook --ip=0.0.0.0 --port=8889 --allow-root`
- DqQvcBsrJ19ETReMcnLdB

### Ansible requirements
- `$ virtualenv env`
- `$ source env/bin/activate`
- `$ pip3 install ansible`


### Run a playbook
-`$ansible-playbook -i vhosts/hostsBigdata.yml docker_ubuntu_playbook.yml -l bigDataVserver`

### copy ssh id
- `ssh-copy-id -i ansible/keys/id_bdata root@67.205.179.232`

### Download and copy data
- `./downloadDataCP.sh`

### screen
- `screen`
- `ctrl+a+d`
- `screen -r id_session`

## TODO
- `https://blog.newnius.com/setup-distributed-hadoop-cluster-with-docker-step-by-step.html`
- `https://www.codementor.io/@tirthajyotisarkar/how-to-set-up-pyspark-for-your-jupyter-notebook-p8dcfaxhz`
- `ihttps://downloads.apache.org/incubator/livy/0.7.0-incubating/apache-livy-0.7.0-incubating-bin.zip`
- `https://docs.docker.com/engine/reference/commandline/swarm_init/`


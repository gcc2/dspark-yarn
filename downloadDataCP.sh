#!/bin/bash
tar xzvf data-analysis/GlobalLandTemperatureAndElevationByCity.tar.gz
docker cp elevationsByCity.csv mycluster-master:/
docker cp GlobalLandTemperaturesByCity.csv mycluster-master:/
docker cp data-analysis/eda_v4.ipynb mycluster-master:/home/
docker cp data-analysis/graficas.ipynb mycluster-master:/home/
#docker cp data-analysis/prediccion.ipynb mycluster-master:/home/
docker exec -i mycluster-master hdfs dfs -mkdir /datasets
docker exec -i mycluster-master hdfs dfs -put elevationsByCity.csv /datasets
docker exec -i mycluster-master hdfs dfs -put GlobalLandTemperaturesByCity.csv /datasets
exit 0
